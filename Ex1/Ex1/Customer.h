#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	//get and set functions
	string getName() const;
	set<Item> getItems() const;

	void setName(const string name);
	void setItems(const set<Item> items);
private:
	string _name;
	set<Item> _items;


};
