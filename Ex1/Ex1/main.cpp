#include"Customer.h"
#include<map>
#define PRODUCTES 10

void printWelcome();
void printProductes(Item items[], int size);
void modifyItems(bool buy, Item items[], Customer& customer);
void addCustomer(map<string, Customer>& abcCustomers, Item items[]);
void updateCustomer(map<string, Customer>& abcCustomers, Item items[]);
void printBiggestPrice(map<string, Customer>& abcCustomers);

enum options{BUY = 1, CUSTOMER, PRINT, EXIT};
enum option2{ADD =1, REMOVE, BACK};
int main()
{

	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	int choice = 0;
	while(choice != EXIT)
	{
		system("cls");
		printWelcome();
		cin >> choice;
		switch (choice)
		{

		system("cls");
		case BUY:
			addCustomer(abcCustomers, itemList);
			break;
		case CUSTOMER:
			updateCustomer(abcCustomers, itemList);
			break;
		case PRINT:
			printBiggestPrice(abcCustomers);
			break;
		default:
			break;
		}
	}
	

	return 0;
}

/*prints welcome message to customer*/
void printWelcome()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.      to sign as customer and buy items" << endl;
	cout << "2.      to uptade existing customer's items" << endl;
	cout << "3.      to print the customer who pays the most" << endl;
	cout << "4.      to exit" << endl;
}

/*function prints all items in itemList*/
void printProductes(Item items[], int size)
{
	
	for (int i = 0; i < size; i++)
	{
		cout << items[i].getName() << " " << items[i].getSerialNumber() << " " << items[i].getUnitPrice() << endl;
	}
}

/*function to buy and remov items. customer can choose an item from list of items
and add it or remove it if the item exist in it's list*/
void modifyItems(bool buy, Item items[], Customer& customer)
{
	int choice = -1;
	while (choice != 0 && buy == true) //if customer buys items
	{
		system("cls");
		cout << "The items you can buy are: (0 to exit)" << endl;
		printProductes(items, PRODUCTES);
		cout << "What item would you like to buy ? Input: ";
		cin >> choice;
		if (choice > 0 && choice <= PRODUCTES) //if item exists it is added
		{
			customer.addItem(items[choice - 1]);
		}
		else if(choice != 0) 
		{
			cout << "Enter diffrent number!" << endl;
			system("pause");
		}
	}
	while(choice != 0 && !buy) //if customer wants to erase an item
	{
		system("cls");
		cout << "Choose item to remove: (0 to exit)" << endl;
		printProductes(items, PRODUCTES);
		cin >> choice;
		if (choice > 0 && choice <= PRODUCTES) //if item exists it might be erased if it is in customers list
		{
			customer.removeItem(items[choice - 1]);
		}
		else if(choice != 0)
		{
			cout << "Enter diffrent number!" << endl;
			system("pause");
		}
	}
	
}

/*function adds new customer*/
void addCustomer(map<string, Customer>& abcCustomers, Item items[])
{
	string name;
	do 
	{ //get new name that not in abcCustomer 
		cout << "Enter your name: ";
		cin >> name;
	} while (abcCustomers.find(name) != abcCustomers.end());
	Customer customer(name); //create new customer
	modifyItems(true, items, customer); //add items to customer list
	
	abcCustomers.insert(pair<string, Customer>(name, customer)); 
}

/*function updates a customer */
void updateCustomer(map<string, Customer>& abcCustomers, Item items[])
{
	map<string, Customer>::iterator it;
	string name;
	cout << "Enter your name: "; //get name to search customer
	cin >> name;
	if ((it = abcCustomers.find(name)) != abcCustomers.end()) //if customer exists
	{
		int choice = -1;
		while (choice != BACK)
		{
			system("cls");
			cout << "1.Add items\n2.Remove items\n3.Back to menu\n";
			cin >> choice;
			switch (choice)
			{
			case ADD: //add items to customer
				modifyItems(true, items, it->second);
				break;
			case REMOVE: //remove items from customer
				modifyItems(false, items, it->second);
			
			default:
				break;
			}
		}
	}
	else
	{
		cout << "Cutomer does not exist!!" << endl;
		system("pause");
	}
	
}

/*function finds who is the customer that pays the most and prints his name and the price*/
void printBiggestPrice(map<string, Customer>& abcCustomers)
{
	map<string, Customer>::iterator it;
	double biggestPrice = 0;
	string name;
	for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
	{
		if (it->second.totalSum() > biggestPrice)
		{
			biggestPrice = it->second.totalSum(); //save biggest price and name of customer
			name = it->first;
		}
	}
	cout << "The person who paid the most is " << name << " and the price is: " << biggestPrice << endl;
	system("pause");
}