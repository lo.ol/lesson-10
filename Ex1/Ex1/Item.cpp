#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice) 
{
	this->_name = name;
	if (serialNumber.length() == SIZE_OF_SERIALNUM && unitPrice > 0)
	{
		this->_serialNumber = serialNumber;
		this->_unitPrice = unitPrice;
	}
	this->_count = 1;
	
}

Item::~Item()
{
	_name.clear();
	_serialNumber.clear();
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other) const
{
	if (this->getSerialInt() < other.getSerialInt())
	{
		return true;
	}
	return false;
}

bool Item::operator>(const Item & other) const
{
	if (this->getSerialInt() > other.getSerialInt())
	{
		return true;
	}
	return false;
}

bool Item::operator==(const Item & other) const
{
	if (this->getSerialInt() == other.getSerialInt())
	{
		return true;
	}
	return false;
}

string Item::getName() const
{
	return _name;
}

string Item::getSerialNumber() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

int Item::getSerialInt() const
{
	return this->serialNumInt();
}

void Item::setName(const string name)
{
	this->_name = name;
}

void Item::setSerialNumber(const string serialNumber)
{
	if (serialNumber.length() == SIZE_OF_SERIALNUM)
	{
		this->_serialNumber = serialNumber;
	}
}

void Item::setCount(int count)
{
	this->_count = 1;
}

void Item::setUnitPrice(const double unitPrice)
{
	if (unitPrice > 0)
	{
		this->_unitPrice = unitPrice;
	}
}

int Item::serialNumInt() const
{
	int num = std::stoi(_serialNumber, nullptr, 10);
	
	return num;
}


