#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
	_name.clear();
	_items.clear();
}

double Customer::totalSum() const
{
	double sum = 0;

	std::set<Item>::iterator it;
	for (it = _items.begin(); it != _items.end(); it++)
	{
		sum += it->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	set<Item>::iterator it;
	//check if item already exists, if not it inserted 
	if ((it =_items.find(item)) == _items.end())
	{
		this->_items.insert(item); 
	}
	else
	{//if item exists, 1 added to it's count 
		int count = it->getCount() + 1;
		item.setCount(count);
		//to change count item needs to be erased and inseted again
		_items.erase(item);
		_items.insert(item);
		
	}
	
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator it;
	//check if item already exists
	if ((it = _items.find(item)) != _items.end())
	{
		int count = it->getCount() - 1; //delte 1 from count
		item.setCount(count);
		//to change count item needs to be erased and inseted again
		_items.erase(item);
		if (count > 0) //if item erased at all than it is not inserted again
		{
			_items.insert(item);
		}
	}
	else
	{
		
		cout << "No such item in customer list!!, Choose other item" << endl;
		system("pause");
	}
	
	
}

string Customer::getName() const
{
	return _name;
}

set<Item> Customer::getItems() const
{
	return _items;
}

void Customer::setName(const string name)
{
	this->_name = name;
}

void Customer::setItems(const set<Item> items)
{
	this->_items = items;
}
