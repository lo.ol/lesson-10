#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#define SIZE_OF_SERIALNUM 5
using namespace std;

class Item
{
public:
	Item(string name, string serialNumber, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;
	int getSerialInt() const;

	void setName(const string name);
	void setSerialNumber(const string serialNumber);
	void setCount(int count);
	void setUnitPrice(const double unitPrice);

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
	int serialNumInt() const;
};